#!/bin/bash


start_time=$(date +%s)


dbPwd='CHANGE-ME'
dbUser='CHANGE-ME' 

generate_ssl_certificate() {
    domain="$1"
    folder="$2"
    CERTPATH="/etc/letsencrypt/live/"$domain
    if [ ! -d "$CERTPATH" ]; then
        certbot --apache -d "$domain" -d www."$domain" --agree-tos -n
    else                                                                            
        echo "Certificate for "$domain" domain already exists, skipping certificate request..."
    fi
}

# Function to update Apache virtual host config
update_apache_config() {
    domain="$1"
    folder="$2"
    certpath=`find /etc/letsencrypt/live/ -type d -name "$domain*" -print -quit`
    config_file="/etc/apache2/sites-available/$folder.conf"
    # Update the virtual host config to use SSL
    echo "<VirtualHost *:80>" > $config_file
    echo "   ServerName $domain" >> $config_file
    echo "   ServerAlias www.$domain" >> $config_file
    echo "   DocumentRoot /var/www/html/$folder" >> $config_file
    echo "   <Directory "/var/www/html/$folder">" >> $config_file
    echo "      AllowOverride All" >> $config_file
    echo "   </Directory>" >> $config_file
    echo "   ErrorLog ${APACHE_LOG_DIR}/error.log" >> $config_file
    echo "   CustomLog ${APACHE_LOG_DIR}/access.log combined" >> $config_file
    echo "</VirtualHost>" >> $config_file
}

# Function to update WordPress settings
update_wordpress_settings() {
    domain="$1"
    folder="$2"
    ip4=$(/sbin/ip -o -4 addr list ens3 | awk '{print $4}' | cut -d/ -f1)
    wp_config_file="/var/www/html/$folder/wp-config.php"
    wp_folder="/var/www/html/$folder/"
    wp rewrite structure "/%postname%/" --allow-root --hard --path=$wp_folder
    wp rewrite flush --allow-root --hard --path=$wp_folder

    # Update WordPress settings for HTTPS
    echo "" >> "$wp_config_file"
    echo "define('FORCE_SSL_ADMIN', true);" >> "$wp_config_file"
    echo "define('FORCE_SSL_LOGIN', true);" >> "$wp_config_file"
    echo "define('WP_HOME', 'https://www.$domain');" >> "$wp_config_file"
    echo "define('WP_SITEURL', 'https://www.$domain');" >> "$wp_config_file"
    mysql -u $dbUser --password=$dbPwd -e "use $folder; UPDATE wp_options SET option_value = 'https://www.$domain' WHERE option_id = 1 OR option_id = 2"
    mysql -u $dbUser --password=$dbPwd -e "use $folder; UPDATE wp_postmeta SET meta_value = replace(meta_value, 'http://$ip4/wordpress', 'https://www.$domain');" 
    mysql -u $dbUser --password=$dbPwd -e "use $folder; UPDATE wp_posts SET post_content = replace(post_content, 'http://$ip4/wordpress', 'https://www.$domain');"
    mysql -u $dbUser --password=$dbPwd -e "use $folder; UPDATE wp_posts SET guid = replace(guid, 'http://$ip4/wordpress','https://www.$domain');"
    mysql -u $dbUser --password=$dbPwd -e "use $folder; UPDATE wp_options SET option_value = 'www.$domain' WHERE option_name = 'blogname';"

}

# Function to set up .htaccess for WordPress instance
setup_htaccess() {
    folder="$1"
    htaccess_file="/var/www/html/$folder/.htaccess"
    touch $htaccess_file
    chown www-data:www-data $htaccess_file
    chmod 644 $htaccess_file
}

# Check if the CSV file exists
csv_file="/root/a_deployer.csv"
if [ ! -f "$csv_file" ]; then
    echo "CSV file not found: $csv_file"
    exit 1
fi

# Read the CSV file line by line
while IFS="," read -r domain folder; do
    echo "updating apache config..."
    update_apache_config "$domain" "$folder"
    echo "generating ssl certificate..."
    generate_ssl_certificate "$domain" "$folder"
    echo "updating wordpress configuration..."
    update_wordpress_settings "$domain" "$folder"
    echo "htaccess configuration..."
    setup_htaccess "$folder"
    echo "reloading apache2..."
    cd /etc/apache2/sites-available/ && a2dissite $folder.conf
    systemctl reload apache2
    cd /etc/apache2/sites-available/ && a2ensite $folder.conf
    systemctl reload apache2
done < "$csv_file"

end_time=$(date +%s)
execution_time=$((end_time - start_time))
echo "Script execution time: $execution_time seconds"


