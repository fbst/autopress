#!/usr/bin/env bash

start_time=$(date +%s)

set -Eeuo pipefail

msg() {
  echo >&2 -e "${1-}"
}

if [ "$EUID" -ne 0 ]
then 
  echo "Merci d'exécuter ce script en tant que root ('su' pour changer d'utilisateur)"
  exit
fi
letter='A'
dbPwd='CHANGE-ME'
copyExistingWp='n'
wpToCopyPath='/var/www/html/wordpress'
wpName='wordpress'
wpAdminName='wp-admin'
wpAdminEmail='CHANGE-ME'
wpAdminPwd='CHANGE-ME'
wpNumberStart=1
wpNumberEnd=100
isNumber='^[0-9]+$'
isEmail="^(([A-Za-z0-9]+((\.|\-|\_|\+)?[A-Za-z0-9]?)*[A-Za-z0-9]+)|[A-Za-z0-9]+)@(([A-Za-z0-9]+)+((\.|\-|\_)?([A-Za-z0-9]+)+)*)+\.([A-Za-z]{2,})+$"
ip4=$(/sbin/ip -o -4 addr list ens3 | awk '{print $4}' | cut -d/ -f1)

msg ""
msg "========== Installation/mise à jour des packets PHP habituels =========="
apt install -y apache2 certbot python3-certbot-apache php php-fpm libapache2-mod-php php-mysql mysql-server php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip
certbot register --register-unsafely-without-email --agree-tos --config-dir /etc/letsencrypt
msg ""
msg "========== Configuration du serveur Mysql "
mysql -e "use mysql; UPDATE user SET plugin='mysql_native_password' WHERE User='root';FLUSH PRIVILEGES;"
mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '$dbPwd';FLUSH PRIVILEGES;"
systemctl restart mysql

if  [ "$copyExistingWp" = "Y" ] || [ "$copyExistingWp" = "y" ] 
then
  while [ -z "$wpToCopyPath" ] 
  do
    read -p "Le chemin du WordPress à copier (ex. /var/www/html/wordpress) : " wpToCopyPath

    if  [ -z "$wpToCopyPath" ] 
    then
      msg "Le chemin est requis, merci de réessayer"
    fi
  done
else
  while [ -z "$wpAdminName" ] 
  do
    read -p "Le nom de l'utilisateur admin à créer sur les WordPress : " wpAdminName
    if  [ -z "$wpAdminName" ] 
    then
      msg "Le nom est requis, merci de réessayer"
    fi
  done

  while [ -z "$wpAdminEmail" ] || ! [[ $wpAdminEmail =~ $isEmail ]]
  do
    read -p "L'email de l'utilisateur admin : " wpAdminEmail
    if  [ -z "$wpAdminEmail" ] || ! [[ $wpAdminEmail =~ $isEmail ]]
    then
      msg "Un email au bon format est requis, merci de réessayer"
    fi
  done

  while [ -z "$wpAdminPwd" ] 
  do
    read -p "Le mot de passe l'utilisateur admin : " wpAdminPwd
    if  [ -z "$wpAdminPwd" ] 
    then
      msg "Le mot de passe est requis, merci de réessayer"
    fi
  done
fi

while [ -z "$wpName" ] 
do
  read -p "Le nom des WordPress à créer, il sera utilisé comme nom de dossier sur le serveur et comme nom du site (ex. wordpress) : " wpName
  if  [ -z "$wpName" ] 
  then
    msg "Le nom est requis, merci de réessayer"
  fi
done

while [ -z "$wpNumberStart" ] || ! [[ $wpNumberStart =~ $isNumber ]]
do
  read -p "Le nombre de départ du compteur (10 pour que le premier WordPress créé soit wordpress10 par ex.) : " wpNumberStart
  if  [ -z "$wpNumberStart" ] || ! [[ $wpNumberStart =~ $isNumber ]]
  then
    msg "Un nombre est requis, merci de réessayer"
  fi
done

while [ -z "$wpNumberEnd" ] || ! [[ $wpNumberEnd =~ $isNumber ]] || [ "$wpNumberStart" -gt "$wpNumberEnd" ]
do
  read -p "Le nombre de fin du compteur (100 pour que le dernier WordPress créé soit wordpress100 par ex.) : " wpNumberEnd
  if  [ -z "$wpNumberEnd" ] || ! [[ $wpNumberEnd =~ $isNumber ]]
  then
    msg "Un nombre est requis, merci de réessayer"
  else
    if  [ "$wpNumberStart" -gt "$wpNumberEnd" ]
    then
      msg "Le nombre doit être plus grand que le départ du compteur, merci de réessayer"
    fi
  fi
done

msg "----- Move to /tmp folder"
cd /tmp

msg ""
msg "========== Installation/mise à jour de WP CLI =========="
result=$(curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar)
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp
wp --info

if ! dpkg -l phpmyadmin | grep -q ^ii; then
  msg ""
  msg "========== Installation de phpmyadmin =========="
  debconf-set-selections <<<'phpmyadmin phpmyadmin/dbconfig-install boolean false'
  debconf-set-selections <<<'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
  apt install -yq phpmyadmin

    msg ""
    msg "----- Création de la configuration apache"
    touch /etc/apache2/sites-available/phpmyadmin.conf
  cat << EOF > /etc/apache2/sites-available/phpmyadmin.conf
<Directory /var/www/html/phpmyadmin/>
  AllowOverride All
</Directory>
EOF
  a2ensite phpmyadmin
fi

if ! [ "$copyExistingWp" = "Y" ] && ! [ "$copyExistingWp" = "y" ] 
then
  wpToCopyPath="/var/www/html/wordpress"

  msg ""
  msg "========== Création d'une instance WordPress de base =========="
  msg "----- Téléchargement de WordPress"
  wp core download --path=$wpToCopyPath --skip-content --allow-root --force
  mkdir -p $wpToCopyPath/wp-content/upgrade;
  mkdir -p $wpToCopyPath/wp-content/themes;
  mkdir -p $wpToCopyPath/wp-content/plugins;

  msg ""
  msg "----- Mise en place de la config (wp-config.php)"
  wp config create --allow-root --force --path=$wpToCopyPath --dbname=wordpress --dbuser=root --dbpass=$dbPwd

  msg ""
  msg "----- Création de la base de données"
  wp db create --allow-root --path=$wpToCopyPath

  msg ""
  msg "----- Installation"
  wp --debug core install --allow-root --path=$wpToCopyPath --url="http://$ip4/wordpress" --title=$wpName --admin_user=$wpAdminName --admin_password=$wpAdminPwd --admin_email="$wpAdminEmail"

#  msg ""
#  msg "----- Suppression des pages et posts par défaut"
#  wp post delete $(wp post list --post_type=page,post --format=ids --allow-root --path=$wpToCopyPath) --allow-root --path=$wpToCopyPath --force

#  msg ""
#  msg "----- Structure des permaliens positionnée sur le mode 'Titre de la publication' "
#  wp rewrite structure "/%postname%/" --allow-root --hard --path=$wpToCopyPath
#  wp rewrite flush --allow-root --hard --path=$wpToCopyPath

  msg ""
  msg "----- Installation du thème"
  wp theme install colibri-wp --allow-root --activate --path=$wpToCopyPath

  msg ""
  msg "----- Installation des plugins"
  wp plugin install woocommerce --activate --path=$wpToCopyPath --allow-root
  wp plugin install advanced-google-recaptcha --activate --path=$wpToCopyPath --allow-root
  wp plugin install colibri-page-builder --activate --path=$wpToCopyPath --allow-root
  wp plugin install forminator --activate --path=$wpToCopyPath --allow-root
  wp plugin install internal-links --activate --path=$wpToCopyPath --allow-root
  wp plugin install seo-by-rank-math --activate --path=$wpToCopyPath --allow-root
  wp plugin install google-site-kit --activate --path=$wpToCopyPath --allow-root
  wp plugin install wordpress-mu-domain-mapping --activate --path=$wpToCopyPath --allow-root
  wp plugin install wp-fastest-cache --activate --path=$wpToCopyPath --allow-root
  wp plugin install klaviyo --activate --path=$wpToCopyPath --allow-root
  msg ""
  msg "----- Passage de la propriété des fichiers à l'utilisateur apache (www-data)"
  chown -R www-data:www-data $wpToCopyPath;

  msg ""
  msg "----- Création de la configuration apache"
  touch /etc/apache2/sites-available/wordpress.conf
  cat << EOF > /etc/apache2/sites-available/wordpress.conf
<Directory /var/www/html/wordpress/>
  AllowOverride All
</Directory>
EOF
  a2ensite wordpress.conf
  a2enmod rewrite
fi

wpToCopyName=$(basename $wpToCopyPath)
wp db export "$wpToCopyName.sql" --allow-root --path=$wpToCopyPath

msg ""
msg "========== Copie des WordPress =========="
for (( c=$wpNumberStart; c<=$wpNumberEnd; c++ ))
do 
  wpDestPath=/var/www/html/wordpress$letter$c

  msg ""
  msg "----- Copie des fichiers de $wpToCopyPath vers $wpDestPath"
  rsync -a --info=progress2 $wpToCopyPath/* $wpDestPath --exclude themes --exclude plugins

  msg ""
  msg "----- Création des liens symboliques"
  ln -s $wpToCopyPath/wp-content/plugins/ $wpDestPath/wp-content/;
  ln -s $wpToCopyPath/wp-content/themes/ $wpDestPath/wp-content/;

  msg ""
  msg "----- Passage de la propriété des fichiers à l'utilisateur apache (www-data)"
  chown -R www-data:www-data $wpDestPath

  msg ""
  msg "----- Mise à jour de la configuration (wp-config.php)"
  sed -i "s/$wpToCopyName/wordpress$letter$c/g" $wpDestPath/wp-config.php;
  
  msg ""
  msg "----- Création de la configuration apache"
  cp /etc/apache2/sites-available/$wpToCopyName.conf /etc/apache2/sites-available/wordpress$letter$c.conf;
  sed -i "s/$wpToCopyName/wordpress$letter$c/g" /etc/apache2/sites-available/wordpress$letter$c.conf;
  a2ensite wordpress$letter$c

  msg ""
  msg "----- Création et copie de la base de données"
  mysql -u root --password=$dbPwd -e "CREATE DATABASE wordpress$letter$c CHARACTER SET utf8"
  wp db import "$wpToCopyName.sql" --allow-root --path=$wpDestPath
  wp db query "UPDATE wp_options SET option_value = 'http://$ip4/wordpress$letter$c/' WHERE option_id = 1 OR option_id = 2" --allow-root --path=$wpDestPath
done

msg ""
msg "----- Redémarrage d'apache"
/etc/init.d/apache2 restart
rm "$wpToCopyName.sql"
end_time=$(date +%s)
execution_time=$((end_time - start_time))
echo "Script execution time: $execution_time seconds"



